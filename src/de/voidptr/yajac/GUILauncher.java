package de.voidptr.yajac;

import java.awt.EventQueue;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.voidptr.yajac.gui.ErrorMessageReporter;
import de.voidptr.yajac.gui.MainThread;

public class GUILauncher {

    public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    public final static DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    public final static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {
        logger.setLevel(Level.INFO);
        logger.addHandler(new ErrorMessageReporter());

        MainThread mainThread = new MainThread();
        EventQueue.invokeLater(mainThread);
    }


}