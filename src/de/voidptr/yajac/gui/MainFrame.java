package de.voidptr.yajac.gui;

import static de.voidptr.yajac.GUILauncher.logger;
import static de.voidptr.yajac.util.StringUtils.convertStringToHTMLColor;
import static de.voidptr.yajac.util.StringUtils.linkify;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;

import de.voidptr.yajac.GUILauncher;
import de.voidptr.yajac.gui.MainThread.MessageTextFieldListener;

public class MainFrame extends JFrame {

    private static final String OUTPUT_PANE_HTML_TEMPLATE =
            "<html>" +
            "   <body>" +
            "       <div id=\"content\">" +
            "       </div>" +
            "   </body>" +
            "</html>";

    private static final Image icon = Toolkit.getDefaultToolkit().createImage(
            ClassLoader.getSystemResource("de/voidptr/yajac/icon.png"));

    private static final long serialVersionUID = -3064055866581727509L;

    private final JPanel contentPane;

    private final JTable userTable;
    private final DefaultTableModel userTableModel;
    private final HTMLDocument outputDocument;

    private final JEditorPane outputPane;

    public MainFrame(MessageTextFieldListener messageTextFieldListener) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());
        setContentPane(contentPane);

        this.setIconImage(icon);
        this.setTitle("yaJac");

        outputPane = new JEditorPane();
        outputPane.setEditable(false);
        outputPane.setContentType("text/html; charset=UTF-8");
        outputPane.setText(OUTPUT_PANE_HTML_TEMPLATE);
        outputPane.setPreferredSize(new Dimension(600, 400));
        outputPane.addHyperlinkListener(new LinkOpener());

        JScrollPane outputScrollPane = new JScrollPane(outputPane);

        outputDocument = (HTMLDocument) outputPane.getDocument();

        String[] columnNames = {
                "Username",
                "IP",
                "Port"
        };
        userTable = new JTable(new DefaultTableModel(columnNames, 0));
        userTable.setEnabled(false);
        userTableModel = (DefaultTableModel) userTable.getModel();

        JScrollPane userTableScrollPane = new JScrollPane(userTable);
        userTableScrollPane.setPreferredSize(new Dimension(260, 400));

        JTextField messageTextField = new JTextField();
        messageTextField.addActionListener(messageTextFieldListener);

        Insets defaultInsets = new Insets(1, 1, 1, 1);

        add(
                outputScrollPane,
            new GridBagConstraints(0, 0, 1, 1, 5, 3, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, defaultInsets, 0, 0));
        add(
                userTableScrollPane,
            new GridBagConstraints(1, 0, 1, 1, 5, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.VERTICAL, defaultInsets, 2, 2));
        add(
                messageTextField,
            new GridBagConstraints(0, 1, 2, 1, 1, 1, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, defaultInsets, 0, 0));

        pack();
        setResizable(false);
        setLocationRelativeTo(null);

        messageTextField.requestFocus();
    }

    public void printMessage(final String userName, final String message) {
        // delegate UI update to Thread to avoid blocking
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                StringBuilder stringBuilder =
                        new StringBuilder()
                        .append("<div>")
                        .append("<em>[")
                        .append(GUILauncher.timeFormat.format(Calendar.getInstance().getTime()))
                        .append("]</em> ")
                        .append("<strong style=\"color: ")
                        .append(convertStringToHTMLColor(userName))
                        .append("\">")
                        .append(userName)
                        .append(":</strong> ")
                        .append(linkify(message))
                        .append("</div>");

                Element mainContainer = outputDocument.getElement("content");
                try {
                    outputDocument.insertBeforeEnd(mainContainer, stringBuilder.toString());

                    // scroll to bottom
                    outputPane.setCaretPosition(outputDocument.getLength() - 1);

                } catch (BadLocationException | IOException e) {
                    logger.log(Level.WARNING, "Could not add message to output pane", e);
                }
            }
        });
    }

    public void addUser(final String address, final int port, final String username) {
        // delegate UI update to Thread to avoid blocking
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                String[] rowData = {username, address, Integer.toString(port)};
                userTableModel.addRow(rowData);
            }
        });
    }

    public void removeUser(final String address, final int port) {
        // delegate UI update to Thread to avoid blocking
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                synchronized (userTableModel) {
                    Set<Integer> rowsToRemove = new HashSet<>();
                    for (int row = 0; row < userTableModel.getRowCount(); row++) {
                        if (address.equals(userTableModel.getValueAt(row, 1))
                            && Integer.toString(port).equals(userTableModel.getValueAt(row, 2))) {
                            rowsToRemove.add(row);
                        }
                    }
                    for (Integer row : rowsToRemove) {
                        userTableModel.removeRow(row);
                    }
                }
            }
        });
    }

    public void updateUsername(final String address, final int port, final String username) {
        // delegate UI update to Thread to avoid blocking
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                for (int row = 0; row < userTableModel.getRowCount(); row++) {
                    if (address.equals(userTableModel.getValueAt(row, 1))
                        && Integer.toString(port).equals(userTableModel.getValueAt(row, 2))) {
                        userTableModel.setValueAt(username, row, 0);
                    }
                }
            }
        });
    }

    private class LinkOpener implements HyperlinkListener {

        /*
         * Adapted from http://andydunkel.net/eclipse/java/2011/09/27/java-open-an-url-in-the-standard-browser.html
         */
        private void openUri(URI uri) throws IOException, URISyntaxException {
            if (java.awt.Desktop.isDesktopSupported()) {
                java.awt.Desktop desktop = java.awt.Desktop.getDesktop();

                if (desktop.isSupported(java.awt.Desktop.Action.BROWSE)) {
                    desktop.browse(uri);
                }
            }
        }

        @Override
        public void hyperlinkUpdate(HyperlinkEvent event) {
            if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                try {
                    openUri(event.getURL().toURI());
                } catch (NullPointerException | IOException | URISyntaxException e) {
                    logger.log(Level.WARNING, "Failed to open link", e);
                }
            }
        }

    }

    public void displayListenPort(int listenPort) {
        setTitle("yaJac - listening on port " + listenPort);
    }


}
