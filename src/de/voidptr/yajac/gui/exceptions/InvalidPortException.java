package de.voidptr.yajac.gui.exceptions;

public class InvalidPortException extends Exception {

    private static final long serialVersionUID = 7405300682703485719L;

    private final int portNumber;

    public InvalidPortException(int portNumber) {
        this.portNumber = portNumber;
    }

    public int getPortNumber() {
        return portNumber;
    }

}
