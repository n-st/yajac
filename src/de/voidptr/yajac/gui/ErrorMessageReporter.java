package de.voidptr.yajac.gui;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import javax.swing.JOptionPane;

/**
 * Displays an error message dialog for every {@link Level#SEVERE} log message.
 */
public class ErrorMessageReporter extends Handler {
    @Override
    public void publish(LogRecord record) {
        if (record.getLevel().equals(Level.SEVERE)) {
            JOptionPane.showMessageDialog(null, record.getMessage(), "yaJac", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
    }
}
