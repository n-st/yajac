package de.voidptr.yajac.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;
import java.util.UUID;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import de.voidptr.yajac.gui.exceptions.InvalidPortException;

public class ConnectionFrame extends JFrame {

    private static final Image icon = Toolkit.getDefaultToolkit().createImage(
            ClassLoader.getSystemResource("de/voidptr/yajac/icon.png"));

    private static final long serialVersionUID = -1207881767450736261L;

    private final JPanel contentPane;

    private final JRadioButton newRoomRadioButton;
    private final JRadioButton existingRoomRadioButton;

    private final JRadioButton automaticPortRadioButton;
    private final JRadioButton manualPortRadioButton;

    private final JFormattedTextField portTextField;
    private final JTextField addressTextField;
    private final JTextField usernameTextField;

    private final JButton quitButton;
    private final JButton connectButton;


    public ConnectionFrame(ActionListener actionListener) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contentPane = new JPanel();
        contentPane.setLayout(new GridBagLayout());
        setContentPane(contentPane);

        this.setIconImage(icon);
        this.setTitle("yaJac");

        TextFieldListener textFieldListener = new TextFieldListener();

        JLabel usernameLabel = new JLabel("Username:");
        usernameTextField = new JTextField(15);

        usernameTextField.setText(UUID.randomUUID().toString().substring(0, 8));
        usernameTextField.selectAll();
        usernameTextField.addActionListener(textFieldListener);

        ButtonGroup roomButtons = new ButtonGroup();
        newRoomRadioButton = new JRadioButton("Create new room");
        existingRoomRadioButton = new JRadioButton("Join existing room:");
        roomButtons.add(newRoomRadioButton);
        roomButtons.add(existingRoomRadioButton);
        newRoomRadioButton.setSelected(true);

        ButtonGroup portButtons = new ButtonGroup();
        automaticPortRadioButton = new JRadioButton("Determine port automatically");
        manualPortRadioButton = new JRadioButton("Select port manually:");
        portButtons.add(automaticPortRadioButton);
        portButtons.add(manualPortRadioButton);
        automaticPortRadioButton.setSelected(true);

        portTextField = new JFormattedTextField(new DecimalFormat("####0"));
        portTextField.setColumns(6);
        portTextField.setAlignmentY(RIGHT_ALIGNMENT);
        portTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent arg0) {}

            @Override
            public void focusGained(FocusEvent arg0) {
                manualPortRadioButton.setSelected(true);
            }
        });
        portTextField.addActionListener(textFieldListener);

        addressTextField = new JTextField();
        addressTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusLost(FocusEvent arg0) {}

            @Override
            public void focusGained(FocusEvent arg0) {
                existingRoomRadioButton.setSelected(true);
            }
        });
        addressTextField.addActionListener(textFieldListener);

        // Create the buttons on the bottom
        quitButton = new JButton("Quit");
        connectButton = new JButton("Connect");
        quitButton.addActionListener(actionListener);
        connectButton.addActionListener(actionListener);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(connectButton);
        buttonPanel.add(quitButton);

        Insets defaultInsets = new Insets(5, 5, 5, 10);
        Insets topInsets = new Insets(10, 10, 5, 10);
        Insets indentedInsets = new Insets(5, 25, 5, 10);

        add(
            usernameLabel,
            new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, topInsets, 5, 5));
        add(
            usernameTextField,
            new GridBagConstraints(1, 0, 2, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, topInsets, 5, 5));
        add(
            automaticPortRadioButton,
            new GridBagConstraints(0, 1, 3, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, defaultInsets, 5, 5));
        add(
            manualPortRadioButton,
            new GridBagConstraints(0, 2, 2, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.NONE, defaultInsets, 5, 5));
        add(
            portTextField,
            new GridBagConstraints(2, 2, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.NONE, defaultInsets, 5, 5));
        add(
            newRoomRadioButton,
            new GridBagConstraints(0, 3, 3, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, defaultInsets, 5, 5));
        add(
            existingRoomRadioButton,
            new GridBagConstraints(0, 4, 3, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, defaultInsets, 5, 5));
        add(
            addressTextField,
            new GridBagConstraints(0, 5, 3, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, indentedInsets, 5, 5));
        add(
            buttonPanel,
            new GridBagConstraints(0, 6, 3, 1, 1, 1, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL, defaultInsets, 5, 5));

        pack();
        setResizable(false);
        setLocationRelativeTo(null);
    }

    /**
     * @return the selected port or 0 if the user wants it to be selected automatically.
     */
    public int getPort() throws InvalidPortException {
        if (manualPortRadioButton.isSelected()) {
            int port = Integer.parseInt(portTextField.getText());
            if (port < 1 || port > 65535) {
                throw new InvalidPortException(port);
            } else {
                return port;
            }
        } else {
            return 0;
        }
    }

    /**
     * @return the server address entered by the user or <tt>null</tt> if a new room should be created.
     */
    public String getAddress() {
        if (existingRoomRadioButton.isSelected()) {
            return addressTextField.getText();
        } else {
            return null;
        }
    }

    public String getUsername() {
        return usernameTextField.getText();
    }

    private class TextFieldListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(usernameTextField)) {
                automaticPortRadioButton.requestFocus();
            } else if (e.getSource().equals(portTextField)) {
                existingRoomRadioButton.requestFocus();
            } else if (e.getSource().equals(addressTextField)) {
                connectButton.doClick();
            }
        }

    }

}
