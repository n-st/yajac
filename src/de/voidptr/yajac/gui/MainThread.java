package de.voidptr.yajac.gui;

import static de.voidptr.yajac.GUILauncher.logger;
import static de.voidptr.yajac.util.StringUtils.formatInetAddress;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JTextField;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import de.voidptr.yajac.gui.exceptions.InvalidPortException;
import de.voidptr.yajac.networking.ChatNode;
import de.voidptr.yajac.networking.ChatServer;
import de.voidptr.yajac.networking.ChatServerEventListener;
import de.voidptr.yajac.networking.exceptions.ServerStartFailureException;
import de.voidptr.yajac.networking.transportable.Message;
import de.voidptr.yajac.util.StringUtils;

public class MainThread extends Thread implements ActionListener {

    private final ConnectionFrame connectionFrame;
    MainFrame mainFrame;

    ChatServer server;

    public MainThread() {
        connectionFrame = new ConnectionFrame(this);
    }

    public void run() {
        connectionFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        // determine which frame fired the event and pass it on to the
        // appropriate handling function
        if (event.getSource() instanceof JButton) {
            JButton sourceButton = ((JButton) event.getSource());

            if (connectionFrame != null && connectionFrame.isAncestorOf(sourceButton)) {
                handleConnectionFrameEvent(event.getActionCommand());

            }
        }
    }

    private void handleConnectionFrameEvent(String actionCommand) {
        switch (actionCommand) {
        case "Quit":
            connectionFrame.dispose();
            break;
        case "Connect":
            executeConnectCommand();
            break;
        }
    }

    private void executeConnectCommand() {
        String address = connectionFrame.getAddress();
        if (address == null) {
            startServer();
        } else {
            connectToServer(address);
        }
    }

    private void connectToServer(String address) {
        // split something like server:1234 or ::1:4000 into host and port
        String[] parts = StringUtils.rsplit(address, ":");

        if (parts.length != 2) {
            logger.log(Level.SEVERE, "No node port number given");
            return;
        }

        if (parts[0] == null || parts[0].length() == 0 || parts[1] == null || parts[1].length() == 0) {
            logger.log(Level.SEVERE, "Invalid address");
            return;
        }

        String host = parts[0];

        int port;
        try {
            port = Integer.parseInt(parts[1]);
        } catch (NumberFormatException e) {
            logger.log(Level.SEVERE, "Invalid node port number: " + parts[1]);
            return;
        }

        if (port < 0 || port > 65535) {
            logger.log(Level.SEVERE, "Invalid port number: " + port);
            return;
        }

        startServer();
        server.connectToNode(host, port);
    }

    private void startServer() {
        int portNumber;
        try {
            portNumber = connectionFrame.getPort();
        } catch (InvalidPortException e) {
            logger.log(Level.SEVERE, "Invalid server port number: " + e.getPortNumber());
            return;
        }

        String ourUsername = connectionFrame.getUsername();
        try {
            server = new ChatServer(portNumber, ourUsername, logger);
            server.start();

        } catch (ServerStartFailureException e) {
            logger.log(Level.SEVERE, "Could not start server on port " + portNumber, e.getReason());
            return;
        }

        connectionFrame.setVisible(false);
        mainFrame = new MainFrame(new MessageTextFieldListener());
        mainFrame.setVisible(true);
        mainFrame.displayListenPort(server.getListenPort());

        server.registerEventListener(new UIUpdater());
    }

    /**
     * Adapted from http://stackoverflow.com/a/26318/1114687 and
     * http://alvinalexander.com/java/java-audio-example-java-au-play-sound
     */
    public static synchronized void playNotificationSound() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    InputStream resourceStream = ClassLoader.getSystemResourceAsStream("de/voidptr/yajac/aliendrum.wav");

                    AudioStream audioStream = new AudioStream(resourceStream);
                    AudioPlayer.player.start(audioStream);

                } catch (Exception e) {
                    logger.log(Level.WARNING, "Error while trying to play notification sound.", e);
                }
            }
        }).start();
    }

    private class UIUpdater implements ChatServerEventListener {

        private boolean mainFrameExists() {
            return !(mainFrame == null);
        }


        @Override
        public void onNodeConnected(ChatNode node) {
            if (mainFrameExists()) {
                mainFrame.addUser(formatInetAddress(node.getAddress()), node.getPort(), node.getTheirUsername());
            } else {
                logger.log(Level.WARNING, "Could not add new user to displayed list because the main window does not exist.");
            }
        }

        @Override
        public void onNodeDisconnected(ChatNode node) {
            if (mainFrameExists()) {
                mainFrame.removeUser(formatInetAddress(node.getAddress()), node.getPort());
            } else {
                logger.log(Level.WARNING, "Could not remove user from displayed list because the main window does not exist.");
            }
        }

        @Override
        public void onNodeListChanged(Map<String, ChatNode> newNodeList) {
            // already covered by connected/disconnected notification methods
        }

        @Override
        public void onServerError(Throwable e) {
            // already covered by logger
        }

        @Override
        public void onMessageReceived(ChatNode node, Message message) {
            playNotificationSound();
            if (mainFrameExists()) {
                mainFrame.printMessage(message.getAuthorUsername(), message.getMessage());
            } else {
                logger.log(Level.WARNING, "Could not print message because the main window does not exist.");
            }
        }


        @Override
        public void onUsernameChanged(ChatNode node) {
            if (mainFrameExists()) {
                mainFrame.updateUsername(formatInetAddress(node.getAddress()), node.getPort(), node.getTheirUsername());
            } else {
                logger.log(Level.WARNING, "Could not change username in displayed list because the main window does not exist.");
            }
        }

    }

    public class MessageTextFieldListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof JTextField) {
                JTextField textField = ((JTextField) e.getSource());
                String messageText = textField.getText();
                if (messageText.startsWith("@")) {
                    String messageWithoutAt = messageText.substring(1).toLowerCase();
                    for (String username : server.getNodeList().keySet()) {
                        if (messageWithoutAt.startsWith(username.toLowerCase())) {
                            server.sendMessageToUser(username, messageText);
                        }
                    }
                } else {
                    server.broadcastMessage(messageText);
                }
                mainFrame.printMessage(server.getOwnUsername(), messageText);
                textField.setText("");
            }
        }

    }

}