package de.voidptr.yajac.networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.voidptr.yajac.networking.exceptions.ClientDisconnectedException;
import de.voidptr.yajac.networking.exceptions.ServerStartFailureException;
import de.voidptr.yajac.networking.transportable.ChatNodeAddress;
import de.voidptr.yajac.networking.transportable.ChatNodeAddressList;
import de.voidptr.yajac.networking.transportable.Message;
import de.voidptr.yajac.networking.transportable.ServerPortAnnouncement;
import de.voidptr.yajac.networking.transportable.UsernameAnnouncement;

public class ChatServer extends Thread implements ChatNodeEventListener {

    private final Logger logger;

    private final UUID ownNodeId;
    private String ownUsername = "anonymous";
    private final int ownPort;

    private final ServerSocket serverSocket;
    private final Set<ChatNode> pendingNodes;
    private final Map<String, ChatNode> nodes;
    private final Set<ChatNodeAddress> nodeAddresses;
    private final Set<ChatServerEventListener> eventListeners;

    /**
     * @param listenPort the port this server should listen on.
     * If <tt>0</tt> is given, the system will select one automatically.
     * @throws ServerStartFailureException if the server could not be started
     * for some reason, e.g. because the given <tt>listenPort</tt> is already in
     * use.
     */
    public ChatServer(int listenPort, String ownUsername, Logger logger) throws ServerStartFailureException {
        pendingNodes = new HashSet<>();
        nodes = new HashMap<>();
        eventListeners = new HashSet<>();
        nodeAddresses = new HashSet<>();

        ownNodeId = UUID.randomUUID();
        this.ownUsername = ownUsername;

        this.logger = logger;

        logger.log(Level.FINE, "Starting server on port " + listenPort);
        try {
            serverSocket = new ServerSocket(listenPort);
        } catch (IOException e) {
            throw new ServerStartFailureException(e);
        }

        if (serverSocket.isBound()) {
            logger.log(Level.INFO, "Started server on port " + serverSocket.getLocalPort());
        }
        ownPort = serverSocket.getLocalPort();
    }

    public void connectToNode(String host, int port) {
        logger.log(Level.INFO, "Connecting to node at " + host + " on port " + port);

        Socket nodeSocket;
        try {
            nodeSocket = new Socket(host, port);

        } catch (IOException e) {
            logger.log(Level.SEVERE, "An error occurred while connecting to a node", e);
            notifyListenersOfError(e);
            return;
        }

        addNodeToList(nodeSocket);
    }

    /**
     * Wait for connections in an endless loop.
     */
    @Override
    public void run() {
        while (true) {
            Socket newNodeSocket;

            try {
                // blocks until a node connects
                newNodeSocket = serverSocket.accept();

            } catch (IOException e) {
                logger.log(Level.SEVERE, "An error occurred while waiting for a connection", e);
                notifyListenersOfError(e);

                // skip the rest of this loop iteration since there is no new
                // socket that we could initialize
                continue;
            }

            addNodeToList(newNodeSocket);
        }
    }

    private void addNodeToList(Socket nodeSocket) {
        try {
            ChatNode newNode = new ChatNode(nodeSocket, ownNodeId, ownUsername, ownPort);
            pendingNodes.add(newNode);

            newNode.registerEventListener(this);
            newNode.start();

            logger.log(Level.INFO, "New node connected: " + newNode);

            for (ChatServerEventListener eventListener : eventListeners) {
                eventListener.onNodeConnected(newNode);
                eventListener.onNodeListChanged(nodes);
            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, "An error occurred while processing a new connection", e);
            notifyListenersOfError(e);
        }
    }

    public void broadcastMessage(String message) {
        Message messageObject = new Message(ownNodeId, ownUsername, message);
        broadcastMessageObject(messageObject);
    }

    public void sendMessageToUser(String username, String message) {
        if (nodes.containsKey(username)) {
            Message messageObject = new Message(ownNodeId, ownUsername, message);
            ChatNode node = nodes.get(username);
            try {
                node.sendMessage(messageObject);
            } catch (ClientDisconnectedException e) {
                logger.log(Level.SEVERE, "An error occurred while sending a message to " + node, e);
                notifyListenersOfError(e);
            }
        } else {
            logger.log(Level.WARNING, "Cannot send private message to " + username + ": Username doesn't exist");
        }
    }

    private void broadcastMessageObject(Message messageObject) {
        for (Entry<String, ChatNode> entry : nodes.entrySet()) {
            ChatNode node = entry.getValue();
            if (node.isConnected()) {
                try {
                    node.sendMessage(messageObject);
                } catch (ClientDisconnectedException e) {
                    logger.log(Level.SEVERE, "An error occurred while sending a message to " + node, e);
                    notifyListenersOfError(e);
                }
            }
        }
    }


    private void notifyListenersOfError(Throwable e) {
        for (ChatServerEventListener eventListener : eventListeners) {
            eventListener.onServerError(e);
        }
    }


    public String getOwnUsername() {
        return ownUsername;
    }

    public void setOwnUsername(String ownUsername) {
        this.ownUsername = ownUsername;

        UsernameAnnouncement messageObject = new UsernameAnnouncement(ownNodeId, ownUsername);
        broadcastMessageObject(messageObject);
    }

    public int getListenPort() {
        return serverSocket.getLocalPort();
    }

    public Map<String, ChatNode> getNodeList() {
        return nodes;
    }


    @Override
    public void onNodeDisconnected(ChatNode node) {
        nodes.remove(node.getTheirUsername());
        Iterator<ChatNodeAddress> it = nodeAddresses.iterator();
        while (it.hasNext()) {
            if (it.next().getAssociatedUsername().equals(node.getTheirUsername())) {
                it.remove();
            }
        }

        logger.log(Level.INFO, "Node disconnected: " + node);

        for (ChatServerEventListener eventListener : eventListeners) {
            eventListener.onNodeDisconnected(node);
            eventListener.onNodeListChanged(nodes);
        }
    }


    public void registerEventListener(ChatServerEventListener eventListener) {
        eventListeners.add(eventListener);
    }

    public void unregisterEventListener(ChatServerEventListener eventListener) {
        eventListeners.remove(eventListener);
    }

    @Override
    public void onMessageReceived(ChatNode node, Message message) {
        // Messages are of no interest for the server, so we'll just pass them
        // on to whomever is using the server.
        for (ChatServerEventListener eventListener : eventListeners) {
            eventListener.onMessageReceived(node, message);
        }
    }

    @Override
    public void onInvalidDataReceived(ChatNode node, Throwable e) {
        logger.log(Level.WARNING, "Received invalid data from node " + node + ": " + e);
    }

    @Override
    public void onUsernameAnnouncementReceived(ChatNode node,
            UsernameAnnouncement usernameAnnouncement) {
        if (pendingNodes.contains(node)) {
            // node is still in initialization phase
            pendingNodes.remove(node);
            String newUsername = usernameAnnouncement.getAuthorUsername();
            if (newUsername.equals(ownUsername)
                || nodes.containsKey(newUsername)) {
                // we're already connected to this node
                logger.log(Level.INFO, "Dropping duplicate connection to node " + node);
                node.disconnect();
            } else {
                logger.log(Level.INFO, "New node " + node + " has provided its username: " + newUsername);
                nodes.put(newUsername, node);
            }
        } else {
            logger.log(Level.INFO, "Node " + node + " has changed its username: " +
                    usernameAnnouncement.getAuthorUsername());
        }

        for (ChatServerEventListener eventListener : eventListeners) {
            eventListener.onUsernameChanged(node);
        }
    }


    @Override
    public void onPortAnnouncementReceived(ChatNode chatNode,
            ServerPortAnnouncement portAnnouncement) {
        nodeAddresses.add(
                new ChatNodeAddress(
                        chatNode.getID(),
                        chatNode.getRemoteAddress(),
                        portAnnouncement.getPort(),
                        chatNode.getTheirUsername()));
        broadcastNodeAddresses(new ChatNodeAddressList(nodeAddresses));
    }

    private void broadcastNodeAddresses(ChatNodeAddressList nodeAddresses) {
        for (Entry<String, ChatNode> entry : nodes.entrySet()) {
            ChatNode node = entry.getValue();
            if (node.isConnected()) {
                try {
                    node.sendNodeAddresses(nodeAddresses);
                } catch (ClientDisconnectedException e) {
                    logger.log(Level.SEVERE, "An error occurred while sending the list of node addresses to " + node, e);
                    notifyListenersOfError(e);
                }
            }
        }
    }

    @Override
    public void onNodeAddressesReceived(ChatNode chatNode,
            ChatNodeAddressList nodeAddresses) {
        for (final ChatNodeAddress address : nodeAddresses.getNodeAddresses()) {
            if (!address.getAssociatedUsername().equals(ownUsername)
                && !nodes.containsKey(address.getAssociatedUsername())) {
                connectToNode(address.getHost(), address.getPort());
            }
        }
    }

}