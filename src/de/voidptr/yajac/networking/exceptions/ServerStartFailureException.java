package de.voidptr.yajac.networking.exceptions;

public class ServerStartFailureException extends Exception {

    private static final long serialVersionUID = 2199735241399775525L;

    private Exception reason;

    public ServerStartFailureException(Exception reason) {
        this.reason = reason;
    }

    public Exception getReason() {
        return reason;
    }
}
