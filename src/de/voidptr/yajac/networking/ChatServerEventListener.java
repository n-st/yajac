package de.voidptr.yajac.networking;

import java.util.Map;

import de.voidptr.yajac.networking.transportable.Message;

public interface ChatServerEventListener {

    public void onNodeConnected(ChatNode node);

    public void onNodeDisconnected(ChatNode node);

    public void onNodeListChanged(Map<String, ChatNode> nodes);

    public void onUsernameChanged(ChatNode node);

    public void onServerError(Throwable e);

    public void onMessageReceived(ChatNode node, Message message);

}
