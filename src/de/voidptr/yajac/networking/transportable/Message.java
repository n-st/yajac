package de.voidptr.yajac.networking.transportable;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Message implements Serializable {

    private static final long serialVersionUID = 3146973253331850110L;

    private final UUID id;
    private final UUID originNodeId;
    private final Date creationTime;
    private final String authorUsername;
    private final String message;

    public Message(UUID originNodeId, String authorUsername, String message) {
        this.id = UUID.randomUUID();
        this.originNodeId = originNodeId;
        this.creationTime = Calendar.getInstance().getTime();
        this.authorUsername = authorUsername;
        this.message = message;
    }

    public UUID getId() {
        return id;
    }

    public UUID getOriginNodeId() {
        return originNodeId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public String getMessage() {
        return message;
    }

}
