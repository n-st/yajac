package de.voidptr.yajac.networking.transportable;

import java.io.Serializable;
import java.util.UUID;

public class ServerPortAnnouncement implements Serializable {

    private static final long serialVersionUID = 2281802635800521134L;

    private final UUID id;
    private final UUID nodeId;
    private final String authorUsername;
    private final int port;

    public ServerPortAnnouncement(UUID nodeId, String authorUsername, int port) {
        this.id = UUID.randomUUID();
        this.nodeId = nodeId;
        this.authorUsername = authorUsername;
        this.port = port;
    }

    public UUID getId() {
        return id;
    }

    public UUID getNodeId() {
        return nodeId;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public int getPort() {
        return port;
    }

}
