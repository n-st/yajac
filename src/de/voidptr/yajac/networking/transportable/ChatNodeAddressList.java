package de.voidptr.yajac.networking.transportable;

import java.io.Serializable;
import java.util.Set;

public class ChatNodeAddressList implements Serializable {

    private static final long serialVersionUID = -5519739769342239884L;

    private final Set<ChatNodeAddress> nodeAddresses;

    public ChatNodeAddressList(Set<ChatNodeAddress> nodeAddresses) {
        this.nodeAddresses = nodeAddresses;
    }

    public Set<ChatNodeAddress> getNodeAddresses() {
        return nodeAddresses;
    }

}
