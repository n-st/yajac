package de.voidptr.yajac.networking.transportable;

import java.util.UUID;

public class UsernameAnnouncement extends Message {

    private static final long serialVersionUID = -4967802499475355474L;

    /**
     * Basically an empty {@link Message} that allows the user to change the
     * username other node associate with the current node ID.
     */
    public UsernameAnnouncement(UUID originNodeId, String username) {
        super(originNodeId, username, "");
    }

}
