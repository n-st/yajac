package de.voidptr.yajac.networking.transportable;

import java.io.Serializable;
import java.util.UUID;

public class ChatNodeAddress implements Serializable {

    private static final long serialVersionUID = -8001054781659285054L;

    private final UUID id;
    private final UUID nodeId;
    private final String associatedUsername;
    private final String host;
    private final int port;

    public ChatNodeAddress(UUID nodeId, String host, int port, String associatedUsername) {
        this.id = UUID.randomUUID();
        this.nodeId = nodeId;
        this.host = host;
        this.port = port;
        this.associatedUsername = associatedUsername;
    }

    public UUID getId() {
        return id;
    }

    public UUID getNodeId() {
        return nodeId;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getAssociatedUsername() {
        return associatedUsername;
    }

}
