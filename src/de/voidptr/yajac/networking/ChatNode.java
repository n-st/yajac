package de.voidptr.yajac.networking;

import static de.voidptr.yajac.util.StringUtils.formatInetAddress;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import de.voidptr.yajac.networking.exceptions.ClientDisconnectedException;
import de.voidptr.yajac.networking.transportable.ChatNodeAddressList;
import de.voidptr.yajac.networking.transportable.Message;
import de.voidptr.yajac.networking.transportable.ServerPortAnnouncement;
import de.voidptr.yajac.networking.transportable.UsernameAnnouncement;

public class ChatNode extends Thread {

    private final Socket socket;
    private final Set<ChatNodeEventListener> eventListeners;

    private boolean isConnected = true;

    private final ObjectInputStream inputStream;
    private final ObjectOutputStream outputStream;
    private String theirUsername = "anonymous";
    private String ourUsername = "anonymous";
    private final int ourServerPort;
    private final UUID id;

    ChatNode(Socket socket, UUID ourID, String ourUsername, int ourServerPort) throws IOException {
        eventListeners = new HashSet<>();

        this.id = ourID;
        this.ourUsername = ourUsername;
        this.ourServerPort = ourServerPort;
        this.socket = socket;
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        inputStream = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        // introduce ourself to the communication partner
        try {
            sendUsernameAnnouncement(new UsernameAnnouncement(getID(), ourUsername));
            sendPortAnnouncement(new ServerPortAnnouncement(getID(), ourUsername, ourServerPort));
        } catch (ClientDisconnectedException e) {
            // A notification has already been dispatched to anyone who cares,
            // so we can ignore this Exception and bail out.
            return;
        }

        while (isConnected()) {
            try {
                Object receivedObject = inputStream.readObject();

                parseReceivedObject(receivedObject);

            } catch (ClassNotFoundException e) {
                reportUnknownObjectType(e);

            } catch (IOException e) {
                setDisconnected();
                break;
            }
        }
    }


    private void sendPortAnnouncement(
            ServerPortAnnouncement serverPortAnnouncement) throws ClientDisconnectedException {
        checkIfConnected();

        try {
            outputStream.writeObject(serverPortAnnouncement);
            outputStream.flush();

        } catch (IOException e) {
            setDisconnected();
            throw new ClientDisconnectedException();
        }
    }

    public void sendNodeAddresses(ChatNodeAddressList nodeAddresses) throws ClientDisconnectedException {
        checkIfConnected();

        try {
            outputStream.writeObject(nodeAddresses);
            outputStream.flush();

        } catch (IOException e) {
            setDisconnected();
            throw new ClientDisconnectedException();
        }
    }

    private void parseReceivedObject(Object receivedObject) {
        if (receivedObject instanceof UsernameAnnouncement) {
            theirUsername = ((UsernameAnnouncement) receivedObject).getAuthorUsername();
            for (ChatNodeEventListener eventListener : eventListeners) {
                eventListener.onUsernameAnnouncementReceived(this, (UsernameAnnouncement) receivedObject);
            }

        } else if (receivedObject instanceof Message) {
            for (ChatNodeEventListener eventListener : eventListeners) {
                eventListener.onMessageReceived(this, (Message) receivedObject);
            }

        } else if (receivedObject instanceof ServerPortAnnouncement) {
            for (ChatNodeEventListener eventListener : eventListeners) {
                eventListener.onPortAnnouncementReceived(this, (ServerPortAnnouncement) receivedObject);
            }

        } else if (receivedObject instanceof ChatNodeAddressList) {
            for (ChatNodeEventListener eventListener : eventListeners) {
                eventListener.onNodeAddressesReceived(this, (ChatNodeAddressList) receivedObject);
            }

        } else {
            reportUnknownObjectType(new ClassNotFoundException());
        }
    }

    private void reportUnknownObjectType(Throwable e) {
        for (ChatNodeEventListener eventListener : eventListeners) {
            eventListener.onInvalidDataReceived(this, e);
        }
    }
    public void disconnect() {
        try {
            // closing the InputStream automatically closes the underlying Socket
            inputStream.close();
            setDisconnected();

        } catch (IOException e) {
            setDisconnected();
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    private void checkIfConnected() throws ClientDisconnectedException {
        if (!isConnected()) {
            throw new ClientDisconnectedException();
        }
    }

    private void setDisconnected() {
        if (isConnected) {
            isConnected = false;

            for (ChatNodeEventListener eventListener : eventListeners) {
                eventListener.onNodeDisconnected(this);
            }
        }
    }


    @Override
    public String toString() {
        return String.format(
                "%s:%d",
                formatInetAddress(getAddress()),
                getPort());
    }

    public InetAddress getAddress() {
        return socket.getInetAddress();
    }

    public int getPort() {
        return socket.getPort();
    }

    public String getRemoteAddress() {
        return formatInetAddress(socket.getInetAddress());
    }

    public String getTheirUsername() {
        return theirUsername ;
    }

    public UUID getID() {
        return id;
    }


    public void sendMessage(Message messageObject) throws ClientDisconnectedException {
        checkIfConnected();

        try {
            outputStream.writeObject(messageObject);
            outputStream.flush();

        } catch (IOException e) {
            setDisconnected();
            throw new ClientDisconnectedException();
        }
    }

    public void sendUsernameAnnouncement(UsernameAnnouncement usernameAnnouncement) throws ClientDisconnectedException {
        checkIfConnected();

        try {
            outputStream.writeObject(usernameAnnouncement);
            outputStream.flush();

        } catch (IOException e) {
            setDisconnected();
            throw new ClientDisconnectedException();
        }
    }

    public void registerEventListener(ChatNodeEventListener eventListener) {
        eventListeners.add(eventListener);
    }

    public void unregisterEventListener(ChatNodeEventListener eventListener) {
        eventListeners.remove(eventListener);
    }

}
