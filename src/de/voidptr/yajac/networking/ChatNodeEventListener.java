package de.voidptr.yajac.networking;

import de.voidptr.yajac.networking.transportable.ChatNodeAddressList;
import de.voidptr.yajac.networking.transportable.Message;
import de.voidptr.yajac.networking.transportable.ServerPortAnnouncement;
import de.voidptr.yajac.networking.transportable.UsernameAnnouncement;

interface ChatNodeEventListener {

    public void onMessageReceived(ChatNode node, Message message);

    public void onUsernameAnnouncementReceived(ChatNode node, UsernameAnnouncement usernameAnnouncement);

    public void onNodeDisconnected(ChatNode node);

    public void onInvalidDataReceived(ChatNode node, Throwable e);

    public void onPortAnnouncementReceived(ChatNode chatNode,
            ServerPortAnnouncement portAnnouncement);

    public void onNodeAddressesReceived(ChatNode chatNode,
            ChatNodeAddressList nodeAddresses);

}
