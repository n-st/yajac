package de.voidptr.yajac.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;

public class NetworkUtils {

    public static Collection<InetAddress> getAllLocalAddresses() {
        ArrayList<InetAddress> allAddresses = new ArrayList<InetAddress>();

        Enumeration<NetworkInterface> networkInterfaces;
        try {
            networkInterfaces = NetworkInterface.getNetworkInterfaces();

            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();

                Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    allAddresses.add(addresses.nextElement());
                }
            }
        } catch (SocketException e) {
            // will be an empty list in the worst case
            return allAddresses;
        }

        return allAddresses;
    }

}
