package de.voidptr.yajac.util;

import java.awt.Color;
import java.net.Inet6Address;
import java.net.InetAddress;

public class StringUtils {

    private static final int HASHCODE_MAX_VALUE = 0xFFFFFFF;

    /**
     * @return an array containing
     * <ol>
     *   <li>the input string up to (but not including) the last occurence of the <tt>delimiter</tt></li>
     *   <li>a substring of the input starting on the character after the last occurrence of the <tt>delimiter</tt></li>
     * </ol>
     * or an empty array if <tt>input</tt> doesn't contain <tt>delimiter</tt>.
     */
    public static String[] rsplit(String input, String delimiter) {
        int previousIndex = 0;
        while (input.indexOf(delimiter, previousIndex) != -1) {
            previousIndex = input.indexOf(delimiter, previousIndex) + 1;
        }

        if (previousIndex == 0) {
            // input doesn't contain delimiter
            return new String[] {};
        }

        return new String[] {
                input.substring(0, previousIndex - 1),
                input.substring(previousIndex - 1 + delimiter.length())
        };
    }

    public static String formatInetAddress(InetAddress address) {
        if (address instanceof Inet6Address) {
            return String.format("[%s]", address.getHostAddress());
        } else {
            return address.getHostAddress();
        }
    }

    /**
     * Converts a String to a color in the RGB format used by HTML and CSS.
     * <br>
     * The color is generated using {@link Color#getHSBColor(float, float,
     * float)} and will always have a brightness of 0.5, so it will be legible
     * on a white background.
     *
     * @param str The String to be converted to a color.
     * @return A color in the RGB format used by HTML and CSS, e.g. "#320080".
     *
     * @author Nils Steinger
     * @see "Original implementation: org.sosy_lab.verifiercloud.client.applications.infoclient.formatting"
     * @see http://www.avajava.com/tutorials/lessons/how-do-i-generate-an-md5-digest-for-a-string.html for the MD5 hashing
     */
    public static String convertStringToHTMLColor(String str) {
        float hue = (float) (str.hashCode() & HASHCODE_MAX_VALUE) / HASHCODE_MAX_VALUE;
        Color color = Color.getHSBColor(hue, 1, (float) 0.5);

        // Remove the first byte (which contains the alpha value)
        String rgb = Integer.toHexString(color.getRGB()).substring(2);

        return "#" + rgb;
    }

    /**
     * Replace text of the form <i>anything.domain.tld/directory/file.ext</i>,
     * <i>domain.tld</i> and <i>http(s)/(s)ftp(s)://</i> links with actual HTML anchor
     * tags.
     */
    public static String linkify(String input) {
        return input
                .replaceAll("(\\bs?(ht|f)tps?://[A-Za-z0-9.-/]*[A-Za-z0-9-/]\\b)", "<a href=\"$1\">$1</a>")
                .replaceAll("(\\b(?<!://)[A-Za-z0-9.-]+\\.[a-z]{2,4}(/[A-Za-z0-9./-]*[A-Za-z0-9/-])?\\b)", "<a href=\"http://$1\">$1</a>");
    }

}
